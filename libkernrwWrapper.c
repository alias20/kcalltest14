#include "libkernrwWrapper.h"
#include <dlfcn.h>
#include <mach/mach.h>
#include <stdlib.h>
#include <stdio.h>

static void *handlerKRW = NULL;

static void *handlerKRW_requestKernRw = NULL;
static void *handlerKRW_read32 = NULL;
static void *handlerKRW_read64 = NULL;
static void *handlerKRW_write32 = NULL;
static void *handlerKRW_write64 = NULL;
static void *handlerKRW_readbuf = NULL;
static void *handlerKRW_writebuf = NULL;
static void *handlerKRW_getKernelBase = NULL;
static void *handlerKRW_getKernelProc = NULL;
static void *handlerKRW_getAllProc = NULL;

void getAddrFuncDobby() {
    handlerKRW_requestKernRw = dlsym(handlerKRW, "requestKernRw");
    handlerKRW_read32 = dlsym(handlerKRW, "kernRW_read32");
    handlerKRW_read64 = dlsym(handlerKRW, "kernRW_read64");
    handlerKRW_write32 = dlsym(handlerKRW, "kernRW_write32");
    handlerKRW_write64 = dlsym(handlerKRW, "kernRW_write64");
    handlerKRW_readbuf = dlsym(handlerKRW, "kernRW_readbuf");
    handlerKRW_writebuf = dlsym(handlerKRW, "kernRW_writebuf");
    handlerKRW_getKernelBase = dlsym(handlerKRW, "kernRW_getKernelBase");
    handlerKRW_getKernelProc = dlsym(handlerKRW, "kernRW_getKernelProc");
    handlerKRW_getAllProc = dlsym(handlerKRW, "kernRW_getAllProc");
}

void openKRW() {
    handlerKRW = dlopen("/usr/lib/libkernrw.0.dylib", RTLD_NOW);
    getAddrFuncDobby();
}

void closeKRW() {
    dlclose(handlerKRW);
}

int requestKernRw(void) {
    int (*requestKernRw)(void) = NULL;
    requestKernRw = (int (*)(void))handlerKRW_requestKernRw;
    return requestKernRw();
}

kern_return_t kernRW_read32(uint64_t addr, uint32_t *val) {
    kern_return_t (*kernRW_read32)(uint64_t addr, uint32_t *val) = NULL;
    kernRW_read32 = (kern_return_t (*)(uint64_t addr, uint32_t *val))handlerKRW_read32;
    return kernRW_read32(addr, val);
}

kern_return_t kernRW_read64(uint64_t addr, uint64_t *val) {
    kern_return_t (*kernRW_read64)(uint64_t addr, uint64_t *val) = NULL;
    kernRW_read64 = (kern_return_t (*)(uint64_t addr, uint64_t *val))handlerKRW_read64;
    return kernRW_read64(addr, val);
}

kern_return_t kernRW_write32(uint64_t addr, uint32_t val) {
    kern_return_t (*kernRW_write32)(uint64_t addr, uint32_t val) = NULL;
    kernRW_write32 = (kern_return_t (*)(uint64_t addr, uint32_t val))handlerKRW_write32;
    return kernRW_write32(addr, val);
}

kern_return_t kernRW_write64(uint64_t addr, uint64_t val) {
    kern_return_t (*kernRW_write64)(uint64_t addr, uint64_t val) = NULL;
    kernRW_write64 = (kern_return_t (*)(uint64_t addr, uint64_t val))handlerKRW_write64;
    return kernRW_write64(addr, val);
}

kern_return_t kernRW_readbuf(uint64_t addr, void *buf, size_t len) {
    kern_return_t (*kernRW_readbuf)(uint64_t addr, void *buf, size_t len) = NULL;
    kernRW_readbuf = (kern_return_t (*)(uint64_t addr, void *buf, size_t len))handlerKRW_readbuf;
    return kernRW_readbuf(addr, buf, len);
}

kern_return_t kernRW_writebuf(uint64_t addr, const void *buf, size_t len) {
    kern_return_t (*kernRW_writebuf)(uint64_t addr, const void *buf, size_t len) = NULL;
    kernRW_writebuf = (kern_return_t (*)(uint64_t addr, const void *buf, size_t len))handlerKRW_writebuf;
    return kernRW_writebuf(addr, buf, len);
}

kern_return_t kernRW_getKernelBase(uint64_t *val) {
    kern_return_t (*kernRW_getKernelBase)(uint64_t *val) = NULL;
    kernRW_getKernelBase = (kern_return_t (*)(uint64_t *val))handlerKRW_getKernelBase;
    return kernRW_getKernelBase(val);
}

kern_return_t kernRW_getKernelProc(uint64_t *val) {
    kern_return_t (*kernRW_getKernelProc)(uint64_t *val) = NULL;
    kernRW_getKernelProc = (kern_return_t (*)(uint64_t *val))handlerKRW_getKernelProc;
    return kernRW_getKernelProc(val);
}

kern_return_t kernRW_getAllProc(uint64_t *val) {
    kern_return_t (*kernRW_getAllProc)(uint64_t *val) = NULL;
    kernRW_getAllProc = (kern_return_t (*)(uint64_t *val))handlerKRW_getAllProc;
    return kernRW_getAllProc(val);
}

//read kernel
uint32_t kread32(uint64_t where) {
    uint32_t out;
    kernRW_readbuf(where, &out, sizeof(uint32_t));
    return out;
}
uint64_t kread64(uint64_t where) {
    uint64_t out;
    kernRW_readbuf(where, &out, sizeof(uint64_t));
    return out;
}
//write kernel
void kwrite32(uint64_t where, uint32_t what) {
    uint32_t _what = what;
    kernRW_writebuf(where, &_what, sizeof(uint32_t));
}

void kwrite64(uint64_t where, uint64_t what) {
    uint64_t _what = what;
    kernRW_writebuf(where, &_what, sizeof(uint64_t));
}

void HexDump(uint64_t addr, size_t size) {
    void *data = malloc(size);
    kernRW_readbuf(addr, data, size);
    char ascii[17];
    size_t i, j;
    ascii[16] = '\0';
    for (i = 0; i < size; ++i) {
        if ((i % 16) == 0)
        {
            printf("[0x%016llx+0x%03zx] ", addr, i);
//            printf("[0x%016llx] ", i + addr);
        }
        
        printf("%02X ", ((unsigned char*)data)[i]);
        if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
            ascii[i % 16] = ((unsigned char*)data)[i];
        } else {
            ascii[i % 16] = '.';
        }
        if ((i+1) % 8 == 0 || i+1 == size) {
            printf(" ");
            if ((i+1) % 16 == 0) {
                printf("|  %s \n", ascii);
            } else if (i+1 == size) {
                ascii[(i+1) % 16] = '\0';
                if ((i+1) % 16 <= 8) {
                    printf(" ");
                }
                for (j = (i+1) % 16; j < 16; ++j) {
                    printf("   ");
                }
                printf("|  %s \n", ascii);
            }
        }
    }
    free(data);
}