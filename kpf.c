//
//  mineekpf.h
//  kfd
//
//  Created by Mineek on 06/08/2023.
//

// FIXME: This offsetfinder is not ideal by any means, it's just there to be there and to maybe help people who want to run this for some reason, it's slow, and it can be unreliable.

#import "kpf.h"

static uint64_t textexec_text_addr = 0, textexec_text_size = 0;
static uint64_t prelink_text_addr = 0, prelink_text_size = 0;

static unsigned char *
boyermoore_horspool_memmem(const unsigned char* haystack, size_t hlen,
                           const unsigned char* needle,   size_t nlen)
{
    size_t last, scan = 0;
    size_t bad_char_skip[UCHAR_MAX + 1]; /* Officially called:
                                          * bad character shift */

    /* Sanity checks on the parameters */
    if (nlen <= 0 || !haystack || !needle)
        return NULL;

    /* ---- Preprocess ---- */
    /* Initialize the table to default value */
    /* When a character is encountered that does not occur
     * in the needle, we can safely skip ahead for the whole
     * length of the needle.
     */
    for (scan = 0; scan <= UCHAR_MAX; scan = scan + 1)
        bad_char_skip[scan] = nlen;

    /* C arrays have the first byte at [0], therefore:
     * [nlen - 1] is the last byte of the array. */
    last = nlen - 1;

    /* Then populate it with the analysis of the needle */
    for (scan = 0; scan < last; scan = scan + 1)
        bad_char_skip[needle[scan]] = last - scan;

    /* ---- Do the matching ---- */

    /* Search the haystack, while the needle can still be within it. */
    while (hlen >= nlen)
    {
        /* scan from the end of the needle */
        for (scan = last; haystack[scan] == needle[scan]; scan = scan - 1)
            if (scan == 0) /* If the first byte matches, we've found it. */
                return (void *)haystack;

        /* otherwise, we need to skip some bytes and start again.
           Note that here we are getting the skip value based on the last byte
           of needle, no matter where we didn't match. So if needle is: "abcd"
           then we are skipping based on 'd' and that value will be 4, and
           for "abcdd" we again skip on 'd' but the value will be only 1.
           The alternative of pretending that the mismatched character was
           the last character is slower in the normal case (E.g. finding
           "abcd" in "...azcd..." gives 4 by using 'd' but only
           4-2==2 using 'z'. */
        hlen     -= bad_char_skip[haystack[last]];
        haystack += bad_char_skip[haystack[last]];
    }

    return NULL;
}

void get_kernel_section(uint64_t kernel_base, const char *segment, const char *section, uint64_t *addr_out, uint64_t *size_out)
{
    struct mach_header_64 kernel_header;
    kernRW_readbuf(kernel_base, &kernel_header, sizeof(kernel_header));
    
    uint64_t cmdStart = kernel_base + sizeof(kernel_header);
    uint64_t cmdEnd = cmdStart + kernel_header.sizeofcmds;
    
    uint64_t cmdAddr = cmdStart;
    for(int ci = 0; ci < kernel_header.ncmds && cmdAddr <= cmdEnd; ci++)
    {
        struct segment_command_64 cmd;
        kernRW_readbuf(cmdAddr, &cmd, sizeof(cmd));
        
        if(cmd.cmd == LC_SEGMENT_64)
        {
            uint64_t sectStart = cmdAddr + sizeof(cmd);
            bool finished = false;
            for(int si = 0; si < cmd.nsects; si++)
            {
                uint64_t sectAddr = sectStart + si * sizeof(struct section_64);
                struct section_64 sect;
                kernRW_readbuf(sectAddr, &sect, sizeof(sect));
                
                if (!strcmp(cmd.segname, segment) && !strcmp(sect.sectname, section)) {
                    *addr_out = sect.addr;
                    *size_out = sect.size;
                    finished = true;
                    break;
                }
            }
            if (finished) break;
        }
        
        cmdAddr += cmd.cmdsize;
    }
}

void init_kernel(void) {
    uint64_t kernel_base = 0;
    kernRW_getKernelBase(&kernel_base);

    get_kernel_section(kernel_base, "__TEXT_EXEC", "__text", &textexec_text_addr, &textexec_text_size);
    assert(textexec_text_addr != 0 && textexec_text_size != 0);
    get_kernel_section(kernel_base, "__PLK_TEXT_EXEC", "__text", &prelink_text_addr, &prelink_text_size);
    assert(prelink_text_addr != 0 && prelink_text_size != 0);
}

//https://github.com/xerub/patchfinder64/blob/master/patchfinder64.c#L1213-L1229
uint64_t find_add_x0_x0_0x40_ret(void) {
    static const uint8_t insn[] = { 0x00, 0x00, 0x01, 0x91, 0xc0, 0x03, 0x5f, 0xd6 }; // 0x91010000, 0xD65F03C0
    int current_offset = 0;
    while (current_offset < textexec_text_size) {
        uint8_t* buffer = malloc(0x1000);
        kernRW_readbuf(textexec_text_addr + current_offset, buffer, 0x1000);
        uint8_t *str;
        str = boyermoore_horspool_memmem(buffer, 0x1000, insn, sizeof(insn));
        if (str) {
            return str - buffer + textexec_text_addr + current_offset;
        }
        current_offset += 0x1000;
    }
    current_offset = 0;
    while (current_offset < prelink_text_size) {
        uint8_t* buffer = malloc(0x1000);
        kernRW_readbuf(prelink_text_addr + current_offset, buffer, 0x1000);
        uint8_t *str;
        str = boyermoore_horspool_memmem(buffer, 0x1000, insn, sizeof(insn));
        if (str) {
            return str - buffer + prelink_text_addr + current_offset;
        }
        current_offset += 0x1000;
    }
    return 0;
}

uint64_t bof64(uint64_t ptr) {
    for (; ptr >= 0; ptr -= 4) {
        uint32_t op;
        kernRW_readbuf((uint64_t)ptr, &op, 4);
        if ((op & 0xffc003ff) == 0x910003FD) {
            unsigned delta = (op >> 10) & 0xfff;
            if ((delta & 0xf) == 0) {
                uint64_t prev = ptr - ((delta >> 4) + 1) * 4;
                uint32_t au;
                kernRW_readbuf((uint64_t)prev, &au, 4);
                if ((au & 0xffc003e0) == 0xa98003e0) {
                    return prev;
                }
                while (ptr > 0) {
                    ptr -= 4;
                    kernRW_readbuf((uint64_t)ptr, &au, 4);
                    if ((au & 0xffc003ff) == 0xD10003ff && ((au >> 10) & 0xfff) == delta + 0x10) {
                        return ptr;
                    }
                    if ((au & 0xffc003e0) != 0xa90003e0) {
                        ptr += 4;
                        break;
                    }
                }
            }
        }
    }
    return 0;
}
