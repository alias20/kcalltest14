#include <stdio.h>

extern uint32_t off_p_pid;
extern uint32_t off_p_task;
extern uint32_t off_p_list_le_prev;
extern uint32_t off_task_itk_space;
extern uint32_t off_ipc_space_is_table;
extern uint64_t off_add_x0_x0_0x40_ret_func;
extern uint64_t off_empty_kdata_page;
extern uint64_t off_ret_300;
extern uint64_t off_current_proc;
extern uint64_t off_current_task;
extern uint64_t off_kalloc_external;
extern uint64_t off_kfree;

void _offsets_init(void);