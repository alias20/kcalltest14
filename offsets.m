#include "offsets.h"
#include <UIKit/UIKit.h>
#include <Foundation/Foundation.h>

uint32_t off_p_pid = 0;
uint32_t off_p_task = 0;
uint32_t off_p_list_le_prev = 0;
uint32_t off_task_itk_space = 0;
uint32_t off_ipc_space_is_table = 0;
uint64_t off_add_x0_x0_0x40_ret_func = 0;
uint64_t off_empty_kdata_page = 0;
uint64_t off_ret_300 = 0;
uint64_t off_current_proc = 0;
uint64_t off_current_task = 0;
uint64_t off_kalloc_external = 0;
uint64_t off_kfree = 0;

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

void _offsets_init(void) {
    if (SYSTEM_VERSION_EQUAL_TO(@"14.4.2")) {
        printf("[i] offsets selected for iOS 14.4.2\n");

        off_p_pid = 0x68;
        off_p_task = 0x10;
        off_p_list_le_prev = 0x8;

        off_task_itk_space = 0x330;

        off_ipc_space_is_table = 0x20;

        off_add_x0_x0_0x40_ret_func = 0xFFFFFFF007A9EEBC;

        off_empty_kdata_page = 0xFFFFFFF009170000 + 0x100;

        off_ret_300 = 0xFFFFFFF007D52960;

        off_current_proc = 0xFFFFFFF007F78E18;

        off_current_task = 0xFFFFFFF007A8ED24;

        off_kalloc_external = 0xFFFFFFF007A6182C;

        off_kfree = 0xFFFFFFF007A618E8;

    } else {
        printf("[-] No matching offsets.\n");
        exit(EXIT_FAILURE);
    }
}