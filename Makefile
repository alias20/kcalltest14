DEBUG=0
FINALPACKAGE=1
GO_EASY_ON_ME=1

TARGET = iphone:14.5:14.5
ARCHS = arm64

THEOS_DEVICE_IP = 127.0.0.1 -p 2222

include $(THEOS)/makefiles/common.mk

TOOL_NAME = kcalltest14

kcalltest14_FILES = kpf.c offsets.m libkernrwWrapper.c main.m
kcalltest14_FRAMEWORKS = IOKit
kcalltest14_CFLAGS = -fobjc-arc
kcalltest14_CODESIGN_FLAGS = -Sentitlements.plist
kcalltest14_INSTALL_PATH = /usr/local/bin

include $(THEOS_MAKE_PATH)/tool.mk
