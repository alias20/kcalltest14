#import <mach/mach.h>
#import <mach-o/dyld.h>
#import <mach-o/getsect.h>
#import <mach-o/loader.h>
#import <mach-o/nlist.h>
#import <mach-o/reloc.h>
#import <limits.h>
#import <assert.h>
#import <stdlib.h>
#import "libkernrwWrapper.h"

void init_kernel(void);
uint64_t find_add_x0_x0_0x40_ret(void);